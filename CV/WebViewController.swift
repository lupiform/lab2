import UIKit

class WebViewController: UIViewController {
    
    @IBOutlet weak var WebView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: "http://www.stackoverflow.com")
        let urlRequest = URLRequest(url: url!)
        
        WebView.loadRequest(urlRequest)


    }
}
