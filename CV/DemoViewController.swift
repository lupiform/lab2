
//  Copyright © 2017 Camilla. All rights reserved.

import UIKit
import Foundation
class DemoViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    @IBAction func pulseButton(_ sender: UIButton) {
        pulsate()
}
    
    func pulsate(){
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.6
        pulse.fromValue = 0.95
        pulse.toValue = 1.0
        pulse.autoreverses = true
        pulse.repeatCount = 2
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        
        self.button.layer.add(pulse, forKey: "pulse")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.button.layer.cornerRadius = self.button.bounds.size.height / 2
        self.button.layer.borderWidth = 3.0
        self.button.layer.borderColor = UIColor.white.cgColor
        self.button.clipsToBounds = true
        self.button.contentMode = .scaleToFill
    
    }
}
