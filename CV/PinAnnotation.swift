//
//  PinAnnotation.swift
//  CV
//
//  Created by user131924 on 10/27/17.
//  Copyright © 2017 Camilla. All rights reserved.
//

import MapKit
class PinAnnotaion : NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    
    init(coordinate : CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
}
