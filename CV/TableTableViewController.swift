//
//  TableTableViewController.swift
//  CV
//
//  Created by user131924 on 10/27/17.
//  Copyright © 2017 Camilla. All rights reserved.
//

import UIKit

var number = 0
var fullName : String = "Camilla Jensen"
var phoneNumber : String = "0703932635"
var emailAddress : String = "camilla.jensen.159@gmail.com"
var address : String = "Smedjegatan 31"
var webPage : String = "www.stackoverflow.com"
var workExp1 : String = "University"
var workExp2 : String = "Kindergarten"
var demo1 : String = "Bouncy Button"
var image : String = "bild1"


class TableTableViewController: UITableViewController {
   
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var fullNameSlot: UILabel!
    @IBOutlet weak var phoneNumberSlot: UITextView!
    @IBOutlet weak var emailAddressSlot: UITextView!
    @IBOutlet weak var addressSlot: UILabel!
    @IBOutlet weak var webPageSlot: UITextView!
    @IBOutlet weak var workExperience1: UILabel!
    @IBOutlet weak var workExperience2: UILabel!
    @IBOutlet weak var demo1Slot: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImage.image = UIImage(named: image)
        fullNameSlot.text = fullName
        phoneNumberSlot.text = "Mobile number : \(phoneNumber)"
        emailAddressSlot.text = "Email: \(emailAddress)"
        addressSlot.text = "Address: \(address)"
        webPageSlot.text = "Web page: \(webPage)"
        workExperience1.text = workExp1
        workExperience2.text = workExp2
        demo1Slot.text = demo1
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: (Any)?){
        if(segue.identifier == "showWorkDetail"  && (sender as AnyObject).tag == 1){
            number = 1
        }
        else if(segue.identifier == "showWorkDetail"  && (sender as AnyObject).tag == 2){
            number = 2
        }
    }
}





