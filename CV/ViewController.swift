//
//  ViewController.swift
//  CV
//
//  Created by user131924 on 10/27/17.
//  Copyright © 2017 Camilla. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {

@IBOutlet weak var MapView: MKMapView!
    
override func viewDidLoad() {
super.viewDidLoad()

let myLocation = CLLocationCoordinate2DMake(57.781823, 14.173093)
        
MapView.setRegion(MKCoordinateRegionMakeWithDistance(myLocation, 1500, 1500), animated: true)
    
let pin = PinAnnotaion(coordinate: myLocation)
    
MapView.addAnnotation(pin)
        
}
override func didReceiveMemoryWarning() {
super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
}


}

